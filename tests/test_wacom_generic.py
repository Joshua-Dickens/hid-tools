#!/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (c) 2017 Benjamin Tissoires <benjamin.tissoires@gmail.com>
# Copyright (c) 2017 Red Hat, Inc.
# Copyright (c) 2020 Wacom Technology Corp.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors:
#     Jason Gerecke <jason.gerecke@wacom.com>

"""
Tests for the Wacom driver generic codepath.

This module tests the function of the Wacom driver's generic codepath.
The generic codepath is used by devices which are not explicitly listed
in the driver's device table. It uses the device's HID descriptor to
decode reports sent by the device.
"""

from .descriptors_wacom import wacom_pth660_v145, wacom_pth660_v150, \
    wacom_pth860_v145, wacom_pth860_v150, \
    wacom_pth460_v105

import attr
from enum import Enum
from hidtools.hut import HUT
from hidtools.hid import HidUnit
import tests.base as base
import libevdev
import pytest

import logging
logger = logging.getLogger('hidtools.test.wacom')


class ProximityState(Enum):
    """
    Enumeration of allowed proximity states.
    """
    # Tool is not able to be sensed by the device
    OUT = 0

    # Tool is close enough to be sensed, but some data may be invalid
    # or inaccurate
    IN_PROXIMITY = 1

    # Tool is close enough to be sensed with high accuracy. All data
    # valid.
    IN_RANGE = 2

    def fill(self, reportdata):
        """Fill a report with approrpiate HID properties/values."""
        reportdata.inrange = self in [ProximityState.IN_RANGE]
        reportdata.wacomsense = self in [ProximityState.IN_PROXIMITY, ProximityState.IN_RANGE]


class ReportData():
    """
    Placeholder for HID report values.
    """
    pass


@attr.s
class Buttons():
    """
    Stylus button state.

    Describes the state of each of the buttons / "side switches" that
    may be present on a stylus. Buttons set to 'None' indicate the
    state is "unchanged" since the previous event.
    """
    primary = attr.ib(default=None)
    secondary = attr.ib(default=None)
    tertiary = attr.ib(default=None)

    @staticmethod
    def clear():
        """Button object with all states cleared."""
        return Buttons(False, False, False)

    def fill(self, reportdata):
        """Fill a report with approrpiate HID properties/values."""
        reportdata.barrelswitch = int(self.primary or 0)
        reportdata.secondarybarrelswitch = int(self.secondary or 0)
        reportdata.b3 = int(self.tertiary or 0)


@attr.s
class ToolID():
    """
    Stylus tool identifiers.

    Contains values used to identify a specific stylus, e.g. its serial
    number and tool-type identifier. Values of ``0`` may sometimes be
    used for the out-of-range condition.
    """
    serial = attr.ib()
    tooltype = attr.ib()

    @staticmethod
    def clear():
        """ToolID object with all fields cleared."""
        return ToolID(0, 0)

    def fill(self, reportdata):
        """Fill a report with approrpiate HID properties/values."""
        reportdata.transducerserialnumber = self.serial & 0xFFFFFFFF
        reportdata.serialhi = (self.serial >> 32) & 0xFFFFFFFF
        reportdata.tooltype = self.tooltype


@attr.s
class PhysRange():
    """
    Range of HID physical values, with units.
    """
    unit = attr.ib()
    min_size = attr.ib()
    max_size = attr.ib()

    CENTIMETER = HidUnit.from_string("SILinear: cm")
    DEGREE = HidUnit.from_string("EnglishRotation: deg")

    def contains(self, field):
        """
        Check if the physical size of the provided field is in range.

        Compare the physical size described by the provided HID field
        against the range of sizes described by this object. This is
        an exclusive range comparison (e.g. 0 cm is not within the
        range 0 cm - 5 cm) and exact unit comparison (e.g. 1 inch is
        not within the range 0 cm - 5 cm).
        """
        phys_size = (field.physical_max - field.physical_min) * 10**(field.unit_exp)
        return field.unit == self.unit.value and \
            phys_size > self.min_size and \
            phys_size < self.max_size


class BaseTablet(base.UHIDTestDevice):
    """
    Skeleton object for all kinds of tablet devices.
    """
    def __init__(self, rdesc, name=None, info=None):
        assert rdesc is not None
        super().__init__(name, 'Pen', input_info=info, rdesc=rdesc)
        self.buttons = Buttons.clear()
        self.toolid = ToolID.clear()
        self.proximity = ProximityState.OUT
        self.offset = 0
        self.ring = -1
        self.ek0 = False

    def match_evdev_rule(self, application, evdev):
        """
        Filter out evdev nodes based on the requested application.

        The Wacom driver may create several device nodes for each USB
        interface device. It is crucial that we run tests with the
        expected device node or things will obviously go off the rails.
        Use the Wacom driver's usual naming conventions to apply a
        sensible default filter.
        """
        if application in ['Pen', 'Pad']:
            return evdev.name.endswith(application)
        else:
            return True

    def create_report(self, x, y, pressure, buttons=None, toolid=None, proximity=None, reportID=None):
        """
        Return an input report for this device.

        :param x: absolute x
        :param y: absolute y
        :param pressure: pressure
        :param buttons: stylus button state. Use ``None`` for unchanged.
        :param toolid: tool identifiers. Use ``None`` for unchanged.
        :param proximity: a ProximityState indicating the sensor's ability
             to detect and report attributes of this tool. Use ``None``
             for unchanged.
        :param reportID: the numeric report ID for this report, if needed
        """
        if buttons is not None:
            self.buttons = buttons
        buttons = self.buttons

        if toolid is not None:
            self.toolid = toolid
        toolid = self.toolid

        if proximity is not None:
            self.proximity = proximity
        proximity = self.proximity

        reportID = reportID or self.default_reportID

        report = ReportData()
        report.x = x
        report.y = y
        report.tippressure = pressure
        report.tipswitch = pressure > 0
        buttons.fill(report)
        proximity.fill(report)
        toolid.fill(report)

        return super().create_report(report, reportID=reportID)

    def create_report_heartbeat(self, reportID):
        """
        Return a heartbeat input report for this device.

        Heartbeat reports generally contain battery status information,
        among other things.
        """
        report = ReportData()
        report.wacombatterycharging = 1
        return super().create_report(report, reportID=reportID)

    def create_report_pad(self, reportID, ring, ek0):
        report = ReportData()

        if ring is not None:
            self.ring = ring
        ring = self.ring

        if ek0 is not None:
            self.ek0 = ek0
        ek0 = self.ek0

        if ring >= 0:
            report.wacomtouchring = ring
            report.wacomtouchringstatus = 1
        else:
            report.wacomtouchring = 0x7f
            report.wacomtouchringstatus = 0

        report.wacomexpresskey00 = ek0
        return super().create_report(report, reportID=reportID)

    def event(self, x, y, pressure, buttons=None, toolid=None, proximity=None):
        """
        Send an input event on the default report ID.

        :param x: absolute x
        :param y: absolute y
        :param buttons: stylus button state. Use ``None`` for unchanged.
        :param toolid: tool identifiers. Use ``None`` for unchanged.
        :param proximity: a ProximityState indicating the sensor's ability
             to detect and report attributes of this tool. Use ``None``
             for unchanged.
        """
        r = self.create_report(x, y, pressure, buttons, toolid, proximity)
        self.call_input_event(r)
        return [r]

    def event_heartbeat(self, reportID):
        """
        Send a heartbeat event on the requested report ID.
        """
        r = self.create_report_heartbeat(reportID)
        self.call_input_event(r)
        return [r]

    def event_pad(self, reportID, ring=None, ek0=None):
        """
        Send a pad event on the requested report ID.
        """
        r = self.create_report_pad(reportID, ring, ek0)
        self.call_input_event(r)
        return [r]

    def get_report(self, req, rnum, rtype):
        if rtype != self.UHID_FEATURE_REPORT:
            return (1, [])

        rdesc = None
        for v in self.parsed_rdesc.feature_reports.values():
            if v.report_ID == rnum:
                rdesc = v

        if rdesc is None:
            return (1, [])

        result = (1, [])
        result = self.create_report_offset(rdesc) or result
        return result

    def create_report_offset(self, rdesc):
        require = ['Wacom Offset Left', 'Wacom Offset Top', 'Wacom Offset Right', 'Wacom Offset Bottom']
        if not set(require).issubset(set([f.usage_name for f in rdesc])):
            return None

        report = ReportData()
        report.wacomoffsetleft = self.offset
        report.wacomoffsettop = self.offset
        report.wacomoffsetright = self.offset
        report.wacomoffsetbottom = self.offset
        r = rdesc.create_report([report], None)
        return (0, r)


class OpaqueTablet(BaseTablet):
    """
    Bare-bones opaque tablet with a minimum of features.

    A tablet stripped down to its absolute core. It is capable of
    reporting X/Y position and if the pen is in contact. No pressure,
    no barrel switches, no eraser. Notably it *does* report an "In
    Range" flag, but this is only because the Wacom driver expects
    one to function properly. The device uses only standard HID usages,
    not any of Wacom's vendor-defined pages.
    """
    report_descriptor = [
        0x05, 0x0D,                     # . Usage Page (Digitizer),
        0x09, 0x01,                     # . Usage (Digitizer),
        0xA1, 0x01,                     # . Collection (Application),
        0x85, 0x01,                     # .     Report ID (1),
        0x09, 0x20,                     # .     Usage (Stylus),
        0xA1, 0x00,                     # .     Collection (Physical),
        0x09, 0x42,                     # .         Usage (Tip Switch),
        0x09, 0x32,                     # .         Usage (In Range),
        0x15, 0x00,                     # .         Logical Minimum (0),
        0x25, 0x01,                     # .         Logical Maximum (1),
        0x75, 0x01,                     # .         Report Size (1),
        0x95, 0x02,                     # .         Report Count (2),
        0x81, 0x02,                     # .         Input (Variable),
        0x95, 0x06,                     # .         Report Count (6),
        0x81, 0x03,                     # .         Input (Constant, Variable),
        0x05, 0x01,                     # .         Usage Page (Desktop),
        0x09, 0x30,                     # .         Usage (X),
        0x27, 0x80, 0x3E, 0x00, 0x00,   # .         Logical Maximum (16000),
        0x47, 0x80, 0x3E, 0x00, 0x00,   # .         Physical Maximum (16000),
        0x65, 0x11,                     # .         Unit (Centimeter),
        0x55, 0x0D,                     # .         Unit Exponent (13),
        0x75, 0x10,                     # .         Report Size (16),
        0x95, 0x01,                     # .         Report Count (1),
        0x81, 0x02,                     # .         Input (Variable),
        0x09, 0x31,                     # .         Usage (Y),
        0x27, 0x28, 0x23, 0x00, 0x00,   # .         Logical Maximum (9000),
        0x47, 0x28, 0x23, 0x00, 0x00,   # .         Physical Maximum (9000),
        0x81, 0x02,                     # .         Input (Variable),
        0xC0,                           # .     End Collection,
        0xC0,                           # . End Collection,
    ]

    def __init__(self,
                 rdesc=report_descriptor,
                 name=None,
                 info=(0x3, 0x056a, 0x9999)):
        super().__init__(rdesc, name, info)
        self.default_reportID = 1


class OpaqueCTLTablet(BaseTablet):
    """
    Opaque tablet similar to something in the CTL product line.

    A pen-only tablet with most basic features you would expect from
    an actual device. Position, eraser, pressure, barrel buttons.
    Uses the Wacom vendor-defined usage page.
    """
    report_descriptor = [
        0x06, 0x0D, 0xFF,               # . Usage Page (Vnd Wacom Emr),
        0x09, 0x01,                     # . Usage (Digitizer),
        0xA1, 0x01,                     # . Collection (Application),
        0x85, 0x10,                     # .     Report ID (16),
        0x09, 0x20,                     # .     Usage (Stylus),
        0x35, 0x00,                     # .     Physical Minimum (0),
        0x45, 0x00,                     # .     Physical Maximum (0),
        0x15, 0x00,                     # .     Logical Minimum (0),
        0x25, 0x01,                     # .     Logical Maximum (1),
        0xA1, 0x00,                     # .     Collection (Physical),
        0x09, 0x42,                     # .         Usage (Tip Switch),
        0x09, 0x44,                     # .         Usage (Barrel Switch),
        0x09, 0x5A,                     # .         Usage (Secondary Barrel Switch),
        0x09, 0x45,                     # .         Usage (Eraser),
        0x09, 0x3C,                     # .         Usage (Invert),
        0x09, 0x32,                     # .         Usage (In Range),
        0x09, 0x36,                     # .         Usage (In Proximity),
        0x25, 0x01,                     # .         Logical Maximum (1),
        0x75, 0x01,                     # .         Report Size (1),
        0x95, 0x07,                     # .         Report Count (7),
        0x81, 0x02,                     # .         Input (Variable),
        0x95, 0x01,                     # .         Report Count (1),
        0x81, 0x03,                     # .         Input (Constant, Variable),
        0x0A, 0x30, 0x01,               # .         Usage (X),
        0x65, 0x11,                     # .         Unit (Centimeter),
        0x55, 0x0D,                     # .         Unit Exponent (13),
        0x47, 0x80, 0x3E, 0x00, 0x00,   # .         Physical Maximum (16000),
        0x27, 0x80, 0x3E, 0x00, 0x00,   # .         Logical Maximum (16000),
        0x75, 0x18,                     # .         Report Size (24),
        0x95, 0x01,                     # .         Report Count (1),
        0x81, 0x02,                     # .         Input (Variable),
        0x0A, 0x31, 0x01,               # .         Usage (Y),
        0x47, 0x28, 0x23, 0x00, 0x00,   # .         Physical Maximum (9000),
        0x27, 0x28, 0x23, 0x00, 0x00,   # .         Logical Maximum (9000),
        0x81, 0x02,                     # .         Input (Variable),
        0x09, 0x30,                     # .         Usage (Tip Pressure),
        0x55, 0x00,                     # .         Unit Exponent (0),
        0x65, 0x00,                     # .         Unit,
        0x47, 0x00, 0x00, 0x00, 0x00,   # .         Physical Maximum (0),
        0x26, 0xFF, 0x0F,               # .         Logical Maximum (4095),
        0x75, 0x10,                     # .         Report Size (16),
        0x81, 0x02,                     # .         Input (Variable),
        0x75, 0x08,                     # .         Report Size (8),
        0x95, 0x06,                     # .         Report Count (6),
        0x81, 0x03,                     # .         Input (Constant, Variable),
        0x0A, 0x32, 0x01,               # .         Usage (Z),
        0x25, 0x3F,                     # .         Logical Maximum (63),
        0x75, 0x08,                     # .         Report Size (8),
        0x95, 0x01,                     # .         Report Count (1),
        0x81, 0x02,                     # .         Input (Variable),
        0x09, 0x5B,                     # .         Usage (Transducer Serial Number),
        0x09, 0x5C,                     # .         Usage (Transducer Serial Number Hi),
        0x17, 0x00, 0x00, 0x00, 0x80,   # .         Logical Minimum (-2147483648),
        0x27, 0xFF, 0xFF, 0xFF, 0x7F,   # .         Logical Maximum (2147483647),
        0x75, 0x20,                     # .         Report Size (32),
        0x95, 0x02,                     # .         Report Count (2),
        0x81, 0x02,                     # .         Input (Variable),
        0x09, 0x77,                     # .         Usage (Tool Type),
        0x15, 0x00,                     # .         Logical Minimum (0),
        0x26, 0xFF, 0x0F,               # .         Logical Maximum (4095),
        0x75, 0x10,                     # .         Report Size (16),
        0x95, 0x01,                     # .         Report Count (1),
        0x81, 0x02,                     # .         Input (Variable),
        0xC0,                           # .     End Collection,
        0xC0                            # . End Collection
    ]

    def __init__(self,
                 rdesc=report_descriptor,
                 name=None,
                 info=(0x3, 0x056a, 0x9999)):
        super().__init__(rdesc, name, info)
        self.default_reportID = 16


class PTHX60_Pen(BaseTablet):
    """
    Pen interface of a PTH-660 / PTH-860 / PTH-460 tablet.

    This generation of devices are nearly identical to each other, though
    the PTH-460 uses a slightly different descriptor construction (splits
    the pad among several physical collections)
    """
    def __init__(self,
                 rdesc=None,
                 name=None,
                 info=None):
        super().__init__(rdesc, name, info)
        self.default_reportID = 16


class BaseTest:
    class TestTablet(base.BaseTestCase.TestUhid):
        def sync_and_assert_events(self, report, expected_events, auto_syn=True, strict=False):
            """
            Assert we see the expected events in response to a report.
            """
            uhdev = self.uhdev
            syn_event = self.syn_event
            if auto_syn:
                expected_events.append(syn_event)
            actual_events = uhdev.next_sync_events()
            self.debug_reports(report, uhdev, actual_events)
            if strict:
                self.assertInputEvents(expected_events, actual_events)
            else:
                self.assertInputEventsIn(expected_events, actual_events)

        def get_usages(self, uhdev):
            def get_report_usages(report):
                application = report.application
                for field in report.fields:
                    if field.usages is not None:
                        for usage in field.usages:
                            yield (field, usage, application)
                    else:
                        yield(field, field.usage, application)

            desc = uhdev.parsed_rdesc
            reports = [
                *desc.input_reports.values(),
                *desc.feature_reports.values(),
                *desc.output_reports.values(),
            ]
            for report in reports:
                for usage in get_report_usages(report):
                    yield usage

        def assertName(self, uhdev, type):
            """
            Assert that the name is as we expect.

            The Wacom driver applies a number of decorations to the name
            provided by the hardware. We cannot rely on the definition of
            this assertion from the base class to work properly.
            """
            evdev = uhdev.get_evdev()
            expected_name = uhdev.name + type
            if "wacom" not in expected_name.lower():
                expected_name = "Wacom " + expected_name
            assert evdev.name == expected_name

        def test_descriptor_physicals(self):
            """
            Verify that all HID usages which should have a physical range
            actually do, and those which shouldn't don't. Also verify that
            the associated unit is correct and within a sensible range.
            """
            def usage_id(page_name, usage_name):
                page = HUT.usage_page_from_name(page_name)
                return (page.page_id << 16) | page[usage_name].usage

            required = {
                usage_id('Generic Desktop', 'X'): PhysRange(PhysRange.CENTIMETER, 5, 150),
                usage_id('Generic Desktop', 'Y'): PhysRange(PhysRange.CENTIMETER, 5, 150),
                usage_id('Digitizers', 'Width'): PhysRange(PhysRange.CENTIMETER, 5, 150),
                usage_id('Digitizers', 'Height'): PhysRange(PhysRange.CENTIMETER, 5, 150),
                usage_id('Digitizers', 'X Tilt'): PhysRange(PhysRange.DEGREE, 90, 180),
                usage_id('Digitizers', 'Y Tilt'): PhysRange(PhysRange.DEGREE, 90, 180),
                usage_id('Digitizers', 'Twist'): PhysRange(PhysRange.DEGREE, 358, 360),
                usage_id('Wacom', 'X Tilt'): PhysRange(PhysRange.DEGREE, 90, 180),
                usage_id('Wacom', 'Y Tilt'): PhysRange(PhysRange.DEGREE, 90, 180),
                usage_id('Wacom', 'Twist'): PhysRange(PhysRange.DEGREE, 358, 360),
                usage_id('Wacom', 'X'): PhysRange(PhysRange.CENTIMETER, 5, 150),
                usage_id('Wacom', 'Y'): PhysRange(PhysRange.CENTIMETER, 5, 150),
                usage_id('Wacom', 'Wacom TouchRing'): PhysRange(PhysRange.DEGREE, 358, 360),
                usage_id('Wacom', 'Wacom Offset Left'): PhysRange(PhysRange.CENTIMETER, 0, 0.5),
                usage_id('Wacom', 'Wacom Offset Top'): PhysRange(PhysRange.CENTIMETER, 0, 0.5),
                usage_id('Wacom', 'Wacom Offset Right'): PhysRange(PhysRange.CENTIMETER, 0, 0.5),
                usage_id('Wacom', 'Wacom Offset Bottom'): PhysRange(PhysRange.CENTIMETER, 0, 0.5),
            }
            for field, usage, application in self.get_usages(self.uhdev):
                if application == usage_id('Generic Desktop', 'Mouse'):
                    # Ignore the vestigial Mouse collection which exists
                    # on Wacom tablets only for backwards compatibility.
                    continue

                expect_physical = usage in required

                phys_set = field.physical_min != 0 or field.physical_max != 0
                assert phys_set == expect_physical

                unit_set = field.unit != 0
                assert unit_set == expect_physical

                if unit_set:
                    assert required[usage].contains(field)

        def test_prop_direct(self):
            """
            Todo: Verify that INPUT_PROP_DIRECT is set on display devices.
            """
            pass

        def test_prop_pointer(self):
            """
            Todo: Verify that INPUT_PROP_POINTER is set on opaque devices.
            """
            pass


class PenTabletTest(BaseTest.TestTablet):
    def assertName(self, uhdev):
        super().assertName(uhdev, " Pen")


class TouchTabletTest(BaseTest.TestTablet):
    def assertName(self, uhdev):
        super().assertName(uhdev, " Finger")


class TestOpaqueTablet(PenTabletTest):
    def create_device(self):
        return OpaqueTablet()

    def test_sanity(self):
        """
        Bring a pen into contact with the tablet, then remove it.

        Ensure that we get the basic tool/touch/motion events that should
        be sent by the driver.
        """
        uhdev = self.uhdev

        self.sync_and_assert_events(
            uhdev.event(100, 200, pressure=300, buttons=Buttons.clear(), toolid=ToolID(serial=1, tooltype=1), proximity=ProximityState.IN_RANGE),
            [
                libevdev.InputEvent(libevdev.EV_KEY.BTN_TOOL_PEN, 1),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_X, 100),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_Y, 200),
                libevdev.InputEvent(libevdev.EV_KEY.BTN_TOUCH, 1),
            ]
        )

        self.sync_and_assert_events(
            uhdev.event(110, 220, pressure=0),
            [
                libevdev.InputEvent(libevdev.EV_ABS.ABS_X, 110),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_Y, 220),
                libevdev.InputEvent(libevdev.EV_KEY.BTN_TOUCH, 0),
            ]
        )

        self.sync_and_assert_events(
            uhdev.event(120, 230, pressure=0, toolid=ToolID.clear(), proximity=ProximityState.OUT),
            [
                libevdev.InputEvent(libevdev.EV_KEY.BTN_TOOL_PEN, 0),
            ]
        )

        self.sync_and_assert_events(uhdev.event(130, 240, pressure=0), [], auto_syn=False, strict=True)


class TestOpaqueCTLTablet(TestOpaqueTablet):
    def create_device(self):
        return OpaqueCTLTablet()

    def test_buttons(self):
        """
        Test that the barrel buttons (side switches) work as expected.

        Press and release each button individually to verify that we get
        the expected events.
        """
        uhdev = self.uhdev

        self.sync_and_assert_events(
            uhdev.event(100, 200, pressure=0, buttons=Buttons.clear(), toolid=ToolID(serial=1, tooltype=1), proximity=ProximityState.IN_RANGE),
            [
                libevdev.InputEvent(libevdev.EV_KEY.BTN_TOOL_PEN, 1),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_X, 100),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_Y, 200),
                libevdev.InputEvent(libevdev.EV_MSC.MSC_SERIAL, 1),
            ]
        )

        self.sync_and_assert_events(
            uhdev.event(100, 200, pressure=0, buttons=Buttons(primary=True)),
            [
                libevdev.InputEvent(libevdev.EV_KEY.BTN_STYLUS, 1),
                libevdev.InputEvent(libevdev.EV_MSC.MSC_SERIAL, 1),
            ]
        )

        self.sync_and_assert_events(
            uhdev.event(100, 200, pressure=0, buttons=Buttons(primary=False)),
            [
                libevdev.InputEvent(libevdev.EV_KEY.BTN_STYLUS, 0),
                libevdev.InputEvent(libevdev.EV_MSC.MSC_SERIAL, 1),
            ]
        )

        self.sync_and_assert_events(
            uhdev.event(100, 200, pressure=0, buttons=Buttons(secondary=True)),
            [
                libevdev.InputEvent(libevdev.EV_KEY.BTN_STYLUS2, 1),
                libevdev.InputEvent(libevdev.EV_MSC.MSC_SERIAL, 1),
            ]
        )

        self.sync_and_assert_events(
            uhdev.event(100, 200, pressure=0, buttons=Buttons(secondary=False)),
            [
                libevdev.InputEvent(libevdev.EV_KEY.BTN_STYLUS2, 0),
                libevdev.InputEvent(libevdev.EV_MSC.MSC_SERIAL, 1),
            ]
        )


PTHX60_Devices = [
    {"rdesc": wacom_pth660_v145, "info": (0x3, 0x056a, 0x0357)},
    {"rdesc": wacom_pth660_v150, "info": (0x3, 0x056a, 0x0357)},
    {"rdesc": wacom_pth860_v145, "info": (0x3, 0x056a, 0x0358)},
    {"rdesc": wacom_pth860_v150, "info": (0x3, 0x056a, 0x0358)},
    {"rdesc": wacom_pth460_v105, "info": (0x3, 0x056a, 0x0392)},
]

PTHX60_Names = [
    "PTH-660/v145",
    "PTH-660/v150",
    "PTH-860/v145",
    "PTH-860/v150",
    "PTH-460/v105",
]


class TestPTHX60_Pen(TestOpaqueCTLTablet):
    @pytest.fixture(autouse=True, scope="class", params=PTHX60_Devices, ids=PTHX60_Names)
    def set_device_params(self, request):
        request.cls.device_params = request.param

    def create_device(self):
        return PTHX60_Pen(**self.device_params)

    @pytest.mark.xfail
    def test_descriptor_physicals(self):
        # XFAIL: Various documented errata
        super().test_descriptor_physicals()

    def test_heartbeat_spurious(self):
        """
        Test that the heartbeat report does not send spurious events.
        """
        uhdev = self.uhdev

        self.sync_and_assert_events(
            uhdev.event(100, 200, pressure=300, buttons=Buttons.clear(), toolid=ToolID(serial=1, tooltype=0x822), proximity=ProximityState.IN_RANGE),
            [
                libevdev.InputEvent(libevdev.EV_KEY.BTN_TOOL_PEN, 1),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_X, 100),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_Y, 200),
                libevdev.InputEvent(libevdev.EV_KEY.BTN_TOUCH, 1),
            ]
        )

        # Exactly zero events: not even a SYN
        self.sync_and_assert_events(uhdev.event_heartbeat(19), [], auto_syn=False, strict=True)

        self.sync_and_assert_events(
            uhdev.event(110, 200, pressure=300),
            [
                libevdev.InputEvent(libevdev.EV_ABS.ABS_X, 110),
            ]
        )

    def test_empty_pad_sync(self):
        self.empty_pad_sync(num=3, denom=16, reverse=True)

    def empty_pad_sync(self, num, denom, reverse):
        """
        Test that multiple pad collections do not trigger empty syncs.
        """
        def offset_rotation(value):
            """
            Offset touchring rotation values by the same factor as the
            Linux kernel. Tablets historically don't use the same origin
            as HID, and it sometimes changes from tablet to tablet...
            """
            evdev = self.uhdev.get_evdev()
            info = evdev.absinfo[libevdev.EV_ABS.ABS_WHEEL]
            delta = info.maximum - info.minimum + 1
            if reverse:
                value = info.maximum - value
            value += num * delta // denom
            if value > info.maximum:
                value -= delta
            elif value < info.minimum:
                value += delta
            return value

        uhdev = self.uhdev
        uhdev.application = 'Pad'
        evdev = uhdev.get_evdev()

        print(evdev.name)
        self.sync_and_assert_events(
            uhdev.event_pad(reportID=17, ring=0, ek0=1),
            [
                libevdev.InputEvent(libevdev.EV_KEY.BTN_0, 1),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_WHEEL, offset_rotation(0)),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_MISC, 15),
            ]
        )

        self.sync_and_assert_events(
            uhdev.event_pad(reportID=17, ring=1, ek0=1),
            [
                libevdev.InputEvent(libevdev.EV_ABS.ABS_WHEEL, offset_rotation(1))
            ]
        )

        self.sync_and_assert_events(
            uhdev.event_pad(reportID=17, ring=2, ek0=0),
            [
                libevdev.InputEvent(libevdev.EV_ABS.ABS_WHEEL, offset_rotation(2)),
                libevdev.InputEvent(libevdev.EV_KEY.BTN_0, 0),
            ]
        )


class BaseTouchTablet(base.UHIDTestDevice):
    """
    Skeleton object for touch tablet devices.
    """
    def __init__(self, rdesc, name=None, info=None):
        assert rdesc is not None
        super().__init__(name, 'Touch Screen', input_info=info, rdesc=rdesc)
        self.offset = 0

    def create_report(self, x, y, tipswitch, contactid, confidence, width, height, contactcount, reportID=None):
        """
        Return an input report for this device.

        :param x: absolute x
        :param y: absolute y
        :param tipswitch: finger making contact with the screen bit
        :param contactid: ID for the finger the report is referencing
        :param confidence: confidence bit
        :param width: width value
        :param height: height value
        :param contactcount: number of fingers currently in contact
        :param reportID: the numeric report ID for this report, if needed
        """

        reportID = reportID or self.default_reportID

        report = ReportData()
        report.confidence = confidence
        report.contactid = contactid
        report.tipswitch = tipswitch
        report.x = x
        report.y = y
        report.width = width
        report.height = height
        report.contactcount = contactcount
        return super().create_report(report, reportID=reportID)

    def event(self, x, y, tipswitch, contactid, confidence, width, height, contactcount):
        """
        Send an input event on the default report ID.

        :param x: absolute x
        :param y: absolute y
        :param tipswitch: finger making contact with the screen bit
        :param contactid: ID for the finger the report is referencing
        :param confidence: confidence bit
        :param width: width value
        :param height: height value
        :param contactcount: number of fingers currently in contact
        """
        r = self.create_report(x, y, tipswitch, contactid, confidence, width, height, contactcount)
        self.call_input_event(r)
        return [r]


class DTH2452Tablet(BaseTouchTablet):
    """
    DTH-2452 touch descriptor
    Descriptor covers multiple fingers and functions needed for most tests
    """
    report_descriptor = [
        0x05, 0x0d,                    # . Usage Page (Digitizers)             0
        0x09, 0x04,                    # . Usage (Touch Screen)                2
        0xa1, 0x01,                    # . Collection (Application)            4
        0x85, 0x0c,                    # .  Report ID (12)                     6
        0x95, 0x01,                    # .  Report Count (1)                   8
        0x75, 0x08,                    # .  Report Size (8)                    10
        0x15, 0x00,                    # .  Logical Minimum (0)                12
        0x26, 0xff, 0x00,              # .  Logical Maximum (255)              14
        0x81, 0x03,                    # .  Input (Cnst,Var,Abs)               17
        0x09, 0x54,                    # .  Usage (Contact Count)              19
        0x81, 0x02,                    # .  Input (Data,Var,Abs)               21
        0x09, 0x22,                    # .  Usage (Finger)                     23
        0xa1, 0x02,                    # .  Collection (Logical)               25
        0x05, 0x0d,                    # .   Usage Page (Digitizers)           27
        0x95, 0x01,                    # .   Report Count (1)                  29
        0x75, 0x01,                    # .   Report Size (1)                   31
        0x25, 0x01,                    # .   Logical Maximum (1)               33
        0x09, 0x42,                    # .   Usage (Tip Switch)                35
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              37
        0x81, 0x03,                    # .   Input (Cnst,Var,Abs)              39
        0x09, 0x47,                    # .   Usage (Confidence)                41
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              43
        0x95, 0x05,                    # .   Report Count (5)                  45
        0x81, 0x03,                    # .   Input (Cnst,Var,Abs)              47
        0x09, 0x51,                    # .   Usage (Contact Id)                49
        0x26, 0xff, 0x00,              # .   Logical Maximum (255)             51
        0x75, 0x10,                    # .   Report Size (16)                  54
        0x95, 0x01,                    # .   Report Count (1)                  56
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              58
        0x35, 0x00,                    # .   Physical Minimum (0)              60
        0x65, 0x11,                    # .   Unit (Centimeter,SILinear)        62
        0x55, 0x0e,                    # .   Unit Exponent (-2)                64
        0x05, 0x01,                    # .   Usage Page (Generic Desktop)      66
        0x09, 0x30,                    # .   Usage (X)                         68
        0x26, 0xa0, 0x44,              # .   Logical Maximum (17568)           70
        0x46, 0x96, 0x14,              # .   Physical Maximum (5270)           73
        0x81, 0x42,                    # .   Input (Data,Var,Abs,Null)         76
        0x09, 0x31,                    # .   Usage (Y)                         78
        0x26, 0x9a, 0x26,              # .   Logical Maximum (9882)            80
        0x46, 0x95, 0x0b,              # .   Physical Maximum (2965)           83
        0x81, 0x42,                    # .   Input (Data,Var,Abs,Null)         86
        0x05, 0x0d,                    # .   Usage Page (Digitizers)           88
        0x75, 0x08,                    # .   Report Size (8)                   90
        0x95, 0x01,                    # .   Report Count (1)                  92
        0x15, 0x00,                    # .   Logical Minimum (0)               94
        0x09, 0x48,                    # .   Usage (Width)                     96
        0x26, 0x5f, 0x00,              # .   Logical Maximum (95)              98
        0x46, 0x7c, 0x14,              # .   Physical Maximum (5244)           101
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              104
        0x09, 0x49,                    # .   Usage (Height)                    106
        0x25, 0x35,                    # .   Logical Maximum (53)              108
        0x46, 0x7d, 0x0b,              # .   Physical Maximum (2941)           110
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              113
        0x45, 0x00,                    # .   Physical Maximum (0)              115
        0x65, 0x00,                    # .   Unit (None)                       117
        0x55, 0x00,                    # .   Unit Exponent (0)                 119
        0xc0,                          # .  End Collection                     121
        0x05, 0x0d,                    # .  Usage Page (Digitizers)            122
        0x09, 0x22,                    # .  Usage (Finger)                     124
        0xa1, 0x02,                    # .  Collection (Logical)               126
        0x05, 0x0d,                    # .   Usage Page (Digitizers)           128
        0x95, 0x01,                    # .   Report Count (1)                  130
        0x75, 0x01,                    # .   Report Size (1)                   132
        0x25, 0x01,                    # .   Logical Maximum (1)               134
        0x09, 0x42,                    # .   Usage (Tip Switch)                136
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              138
        0x81, 0x03,                    # .   Input (Cnst,Var,Abs)              140
        0x09, 0x47,                    # .   Usage (Confidence)                142
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              144
        0x95, 0x05,                    # .   Report Count (5)                  146
        0x81, 0x03,                    # .   Input (Cnst,Var,Abs)              148
        0x09, 0x51,                    # .   Usage (Contact Id)                150
        0x26, 0xff, 0x00,              # .   Logical Maximum (255)             152
        0x75, 0x10,                    # .   Report Size (16)                  155
        0x95, 0x01,                    # .   Report Count (1)                  157
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              159
        0x35, 0x00,                    # .   Physical Minimum (0)              161
        0x65, 0x11,                    # .   Unit (Centimeter,SILinear)        163
        0x55, 0x0e,                    # .   Unit Exponent (-2)                165
        0x05, 0x01,                    # .   Usage Page (Generic Desktop)      167
        0x09, 0x30,                    # .   Usage (X)                         169
        0x26, 0xa0, 0x44,              # .   Logical Maximum (17568)           171
        0x46, 0x96, 0x14,              # .   Physical Maximum (5270)           174
        0x81, 0x42,                    # .   Input (Data,Var,Abs,Null)         177
        0x09, 0x31,                    # .   Usage (Y)                         179
        0x26, 0x9a, 0x26,              # .   Logical Maximum (9882)            181
        0x46, 0x95, 0x0b,              # .   Physical Maximum (2965)           184
        0x81, 0x42,                    # .   Input (Data,Var,Abs,Null)         187
        0x05, 0x0d,                    # .   Usage Page (Digitizers)           189
        0x75, 0x08,                    # .   Report Size (8)                   191
        0x95, 0x01,                    # .   Report Count (1)                  193
        0x15, 0x00,                    # .   Logical Minimum (0)               195
        0x09, 0x48,                    # .   Usage (Width)                     197
        0x26, 0x5f, 0x00,              # .   Logical Maximum (95)              199
        0x46, 0x7c, 0x14,              # .   Physical Maximum (5244)           202
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              205
        0x09, 0x49,                    # .   Usage (Height)                    207
        0x25, 0x35,                    # .   Logical Maximum (53)              209
        0x46, 0x7d, 0x0b,              # .   Physical Maximum (2941)           211
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              214
        0x45, 0x00,                    # .   Physical Maximum (0)              216
        0x65, 0x00,                    # .   Unit (None)                       218
        0x55, 0x00,                    # .   Unit Exponent (0)                 220
        0xc0,                          # .  End Collection                     222
        0x05, 0x0d,                    # .  Usage Page (Digitizers)            223
        0x09, 0x22,                    # .  Usage (Finger)                     225
        0xa1, 0x02,                    # .  Collection (Logical)               227
        0x05, 0x0d,                    # .   Usage Page (Digitizers)           229
        0x95, 0x01,                    # .   Report Count (1)                  231
        0x75, 0x01,                    # .   Report Size (1)                   233
        0x25, 0x01,                    # .   Logical Maximum (1)               235
        0x09, 0x42,                    # .   Usage (Tip Switch)                237
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              239
        0x81, 0x03,                    # .   Input (Cnst,Var,Abs)              241
        0x09, 0x47,                    # .   Usage (Confidence)                243
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              245
        0x95, 0x05,                    # .   Report Count (5)                  247
        0x81, 0x03,                    # .   Input (Cnst,Var,Abs)              249
        0x09, 0x51,                    # .   Usage (Contact Id)                251
        0x26, 0xff, 0x00,              # .   Logical Maximum (255)             253
        0x75, 0x10,                    # .   Report Size (16)                  256
        0x95, 0x01,                    # .   Report Count (1)                  258
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              260
        0x35, 0x00,                    # .   Physical Minimum (0)              262
        0x65, 0x11,                    # .   Unit (Centimeter,SILinear)        264
        0x55, 0x0e,                    # .   Unit Exponent (-2)                266
        0x05, 0x01,                    # .   Usage Page (Generic Desktop)      268
        0x09, 0x30,                    # .   Usage (X)                         270
        0x26, 0xa0, 0x44,              # .   Logical Maximum (17568)           272
        0x46, 0x96, 0x14,              # .   Physical Maximum (5270)           275
        0x81, 0x42,                    # .   Input (Data,Var,Abs,Null)         278
        0x09, 0x31,                    # .   Usage (Y)                         280
        0x26, 0x9a, 0x26,              # .   Logical Maximum (9882)            282
        0x46, 0x95, 0x0b,              # .   Physical Maximum (2965)           285
        0x81, 0x42,                    # .   Input (Data,Var,Abs,Null)         288
        0x05, 0x0d,                    # .   Usage Page (Digitizers)           290
        0x75, 0x08,                    # .   Report Size (8)                   292
        0x95, 0x01,                    # .   Report Count (1)                  294
        0x15, 0x00,                    # .   Logical Minimum (0)               296
        0x09, 0x48,                    # .   Usage (Width)                     298
        0x26, 0x5f, 0x00,              # .   Logical Maximum (95)              300
        0x46, 0x7c, 0x14,              # .   Physical Maximum (5244)           303
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              306
        0x09, 0x49,                    # .   Usage (Height)                    308
        0x25, 0x35,                    # .   Logical Maximum (53)              310
        0x46, 0x7d, 0x0b,              # .   Physical Maximum (2941)           312
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              315
        0x45, 0x00,                    # .   Physical Maximum (0)              317
        0x65, 0x00,                    # .   Unit (None)                       319
        0x55, 0x00,                    # .   Unit Exponent (0)                 321
        0xc0,                          # .  End Collection                     323
        0x05, 0x0d,                    # .  Usage Page (Digitizers)            324
        0x09, 0x22,                    # .  Usage (Finger)                     326
        0xa1, 0x02,                    # .  Collection (Logical)               328
        0x05, 0x0d,                    # .   Usage Page (Digitizers)           330
        0x95, 0x01,                    # .   Report Count (1)                  332
        0x75, 0x01,                    # .   Report Size (1)                   334
        0x25, 0x01,                    # .   Logical Maximum (1)               336
        0x09, 0x42,                    # .   Usage (Tip Switch)                338
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              340
        0x81, 0x03,                    # .   Input (Cnst,Var,Abs)              342
        0x09, 0x47,                    # .   Usage (Confidence)                344
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              346
        0x95, 0x05,                    # .   Report Count (5)                  348
        0x81, 0x03,                    # .   Input (Cnst,Var,Abs)              350
        0x09, 0x51,                    # .   Usage (Contact Id)                352
        0x26, 0xff, 0x00,              # .   Logical Maximum (255)             354
        0x75, 0x10,                    # .   Report Size (16)                  357
        0x95, 0x01,                    # .   Report Count (1)                  359
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              361
        0x35, 0x00,                    # .   Physical Minimum (0)              363
        0x65, 0x11,                    # .   Unit (Centimeter,SILinear)        365
        0x55, 0x0e,                    # .   Unit Exponent (-2)                367
        0x05, 0x01,                    # .   Usage Page (Generic Desktop)      369
        0x09, 0x30,                    # .   Usage (X)                         371
        0x26, 0xa0, 0x44,              # .   Logical Maximum (17568)           373
        0x46, 0x96, 0x14,              # .   Physical Maximum (5270)           376
        0x81, 0x42,                    # .   Input (Data,Var,Abs,Null)         379
        0x09, 0x31,                    # .   Usage (Y)                         381
        0x26, 0x9a, 0x26,              # .   Logical Maximum (9882)            383
        0x46, 0x95, 0x0b,              # .   Physical Maximum (2965)           386
        0x81, 0x42,                    # .   Input (Data,Var,Abs,Null)         389
        0x05, 0x0d,                    # .   Usage Page (Digitizers)           391
        0x75, 0x08,                    # .   Report Size (8)                   393
        0x95, 0x01,                    # .   Report Count (1)                  395
        0x15, 0x00,                    # .   Logical Minimum (0)               397
        0x09, 0x48,                    # .   Usage (Width)                     399
        0x26, 0x5f, 0x00,              # .   Logical Maximum (95)              401
        0x46, 0x7c, 0x14,              # .   Physical Maximum (5244)           404
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              407
        0x09, 0x49,                    # .   Usage (Height)                    409
        0x25, 0x35,                    # .   Logical Maximum (53)              411
        0x46, 0x7d, 0x0b,              # .   Physical Maximum (2941)           413
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              416
        0x45, 0x00,                    # .   Physical Maximum (0)              418
        0x65, 0x00,                    # .   Unit (None)                       420
        0x55, 0x00,                    # .   Unit Exponent (0)                 422
        0xc0,                          # .  End Collection                     424
        0x05, 0x0d,                    # .  Usage Page (Digitizers)            425
        0x09, 0x22,                    # .  Usage (Finger)                     427
        0xa1, 0x02,                    # .  Collection (Logical)               429
        0x05, 0x0d,                    # .   Usage Page (Digitizers)           431
        0x95, 0x01,                    # .   Report Count (1)                  433
        0x75, 0x01,                    # .   Report Size (1)                   435
        0x25, 0x01,                    # .   Logical Maximum (1)               437
        0x09, 0x42,                    # .   Usage (Tip Switch)                439
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              441
        0x81, 0x03,                    # .   Input (Cnst,Var,Abs)              443
        0x09, 0x47,                    # .   Usage (Confidence)                445
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              447
        0x95, 0x05,                    # .   Report Count (5)                  449
        0x81, 0x03,                    # .   Input (Cnst,Var,Abs)              451
        0x09, 0x51,                    # .   Usage (Contact Id)                453
        0x26, 0xff, 0x00,              # .   Logical Maximum (255)             455
        0x75, 0x10,                    # .   Report Size (16)                  458
        0x95, 0x01,                    # .   Report Count (1)                  460
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              462
        0x35, 0x00,                    # .   Physical Minimum (0)              464
        0x65, 0x11,                    # .   Unit (Centimeter,SILinear)        466
        0x55, 0x0e,                    # .   Unit Exponent (-2)                468
        0x05, 0x01,                    # .   Usage Page (Generic Desktop)      470
        0x09, 0x30,                    # .   Usage (X)                         472
        0x26, 0xa0, 0x44,              # .   Logical Maximum (17568)           474
        0x46, 0x96, 0x14,              # .   Physical Maximum (5270)           477
        0x81, 0x42,                    # .   Input (Data,Var,Abs,Null)         480
        0x09, 0x31,                    # .   Usage (Y)                         482
        0x26, 0x9a, 0x26,              # .   Logical Maximum (9882)            484
        0x46, 0x95, 0x0b,              # .   Physical Maximum (2965)           487
        0x81, 0x42,                    # .   Input (Data,Var,Abs,Null)         490
        0x05, 0x0d,                    # .   Usage Page (Digitizers)           492
        0x75, 0x08,                    # .   Report Size (8)                   494
        0x95, 0x01,                    # .   Report Count (1)                  496
        0x15, 0x00,                    # .   Logical Minimum (0)               498
        0x09, 0x48,                    # .   Usage (Width)                     500
        0x26, 0x5f, 0x00,              # .   Logical Maximum (95)              502
        0x46, 0x7c, 0x14,              # .   Physical Maximum (5244)           505
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              508
        0x09, 0x49,                    # .   Usage (Height)                    510
        0x25, 0x35,                    # .   Logical Maximum (53)              512
        0x46, 0x7d, 0x0b,              # .   Physical Maximum (2941)           514
        0x81, 0x02,                    # .   Input (Data,Var,Abs)              517
        0x45, 0x00,                    # .   Physical Maximum (0)              519
        0x65, 0x00,                    # .   Unit (None)                       521
        0x55, 0x00,                    # .   Unit Exponent (0)                 523
        0xc0,                          # .  End Collection                     525
        0x05, 0x0d,                    # .  Usage Page (Digitizers)            526
        0x27, 0xff, 0xff, 0x00, 0x00,  # .  Logical Maximum (65535)            528
        0x75, 0x10,                    # .  Report Size (16)                   533
        0x95, 0x01,                    # .  Report Count (1)                   535
        0x09, 0x56,                    # .  Usage (Scan Time)                  537
        0x81, 0x02,                    # .  Input (Data,Var,Abs)               539
        0x75, 0x08,                    # .  Report Size (8)                    541
        0x95, 0x0e,                    # .  Report Count (14)                  543
        0x81, 0x03,                    # .  Input (Cnst,Var,Abs)               545
        0x09, 0x55,                    # .  Usage (Contact Max)                547
        0x26, 0xff, 0x00,              # .  Logical Maximum (255)              549
        0x75, 0x08,                    # .  Report Size (8)                    552
        0xb1, 0x02,                    # .  Feature (Data,Var,Abs)             554
        0x85, 0x0a,                    # .  Report ID (10)                     556
        0x06, 0x00, 0xff,              # .  Usage Page (Vendor Defined Page 1) 558
        0x09, 0xc5,                    # .  Usage (Vendor Usage 0xc5)          561
        0x96, 0x00, 0x01,              # .  Report Count (256)                 563
        0xb1, 0x02,                    # .  Feature (Data,Var,Abs)             566
        0xc0,                          # . End Collection                      568
        0x06, 0x00, 0xff,              # . Usage Page (Vendor Defined Page 1)  569
        0x09, 0x01,                    # . Usage (Vendor Usage 1)              572
        0xa1, 0x01,                    # . Collection (Application)            574
        0x09, 0x01,                    # .  Usage (Vendor Usage 1)             576
        0x85, 0x13,                    # .  Report ID (19)                     578
        0x15, 0x00,                    # .  Logical Minimum (0)                580
        0x26, 0xff, 0x00,              # .  Logical Maximum (255)              582
        0x75, 0x08,                    # .  Report Size (8)                    585
        0x95, 0x3f,                    # .  Report Count (63)                  587
        0x81, 0x02,                    # .  Input (Data,Var,Abs)               589
        0x06, 0x00, 0xff,              # .  Usage Page (Vendor Defined Page 1) 591
        0x09, 0x01,                    # .  Usage (Vendor Usage 1)             594
        0x15, 0x00,                    # .  Logical Minimum (0)                596
        0x26, 0xff, 0x00,              # .  Logical Maximum (255)              598
        0x75, 0x08,                    # .  Report Size (8)                    601
        0x95, 0x3f,                    # .  Report Count (63)                  603
        0x91, 0x02,                    # .  Output (Data,Var,Abs)              605
        0xc0,                          # . End Collection                      607
    ]

    def __init__(self,
                 rdesc=report_descriptor,
                 name=None,
                 info=(0x3, 0x056a, 0x0383)):
        super().__init__(rdesc, name, info)
        self.default_reportID = 12


class TestDTH2452Tablet(TouchTabletTest):
    def create_device(self):
        return DTH2452Tablet()

    def test_sanity(self):
        """
        Bring a finger into contact with the tablet, then remove it.

        Ensure that we get the basic tool/touch/motion events that should
        be sent by the driver.
        self, x, y, tipswitch, contactid, confidence, width, height, contactcount
        """
        uhdev = self.uhdev
        self.sync_and_assert_events(
            uhdev.event(100, 200, 1, 1, 1, 1, 1, 1),
            [
                libevdev.InputEvent(libevdev.EV_ABS.ABS_X, 100),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_Y, 200),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_X, 100),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_Y, 200),
                libevdev.InputEvent(libevdev.EV_KEY.BTN_TOUCH, 1),
            ]
        )

        self.sync_and_assert_events(
            uhdev.event(105, 215, 1, 1, 1, 1, 1, 1),
            [
                libevdev.InputEvent(libevdev.EV_ABS.ABS_X, 105),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_Y, 215),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_X, 105),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_Y, 215),
            ]
        )

        self.sync_and_assert_events(
            uhdev.event(110, 220, 0, 1, 1, 1, 1, 1),
            [
                libevdev.InputEvent(libevdev.EV_KEY.BTN_TOUCH, 0),
            ]
        )

    def test_contact_id_0(self):
        """
        Bring a finger in contact with the tablet, then hold it down and remove it.

        Ensure that even with contact ID = 0 which is usually given as an invalid
        touch event by most tablets with the exception of a few, that given the
        confidence bit is set to 1 it should process it as a valid touch to cover
        the few tablets using contact ID = 0 as a valid touch value.
        """
        uhdev = self.uhdev
        self.sync_and_assert_events(
            uhdev.event(100, 200, 1, 0, 1, 1, 1, 1),
            [
                libevdev.InputEvent(libevdev.EV_ABS.ABS_X, 100),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_Y, 200),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_X, 100),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_Y, 200),
                libevdev.InputEvent(libevdev.EV_KEY.BTN_TOUCH, 1),
            ]
        )

        self.sync_and_assert_events(
            uhdev.event(101, 201, 1, 0, 1, 1, 1, 1),
            [
                libevdev.InputEvent(libevdev.EV_ABS.ABS_X, 101),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_Y, 201),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_X, 101),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_Y, 201),
            ]
        )

        self.sync_and_assert_events(
            uhdev.event(100, 200, 1, 0, 1, 1, 1, 1),
            [
                libevdev.InputEvent(libevdev.EV_ABS.ABS_X, 100),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_Y, 200),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_X, 100),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_Y, 200),
            ]
        )

        self.sync_and_assert_events(
            uhdev.event(101, 201, 1, 0, 1, 1, 1, 1),
            [
                libevdev.InputEvent(libevdev.EV_ABS.ABS_X, 101),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_Y, 201),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_X, 101),
                libevdev.InputEvent(libevdev.EV_ABS.ABS_MT_POSITION_Y, 201),
            ]
        )

        self.sync_and_assert_events(
            uhdev.event(100, 200, 0, 0, 1, 1, 1, 1),
            [
                libevdev.InputEvent(libevdev.EV_KEY.BTN_TOUCH, 0),
            ]
        )
